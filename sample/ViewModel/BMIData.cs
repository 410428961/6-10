﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace sample.ViewModel
{
    public class BMIData
    {

        [Required(ErrorMessage = "必填欄位")]
        [Range(30,150, ErrorMessage = "30~150")]
        public float Weight { get; set; }

        [Required(ErrorMessage = "必填欄位")]
        [Range(50, 250, ErrorMessage = "50~250")]
        public float Height { get; set; }
        public float BMI { get; set; }
        public string Level { get; set; }
    }
}